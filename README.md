# Prueba
### 1.- Implementar los 4 principios SOLID en java.
### 2.- Implementar en java la búsqueda del valor 51
```
[1, 3, 13, 15, 17, 19, 21, 23, 31, 34, 40, 51, 54, 68]
```



### 4.- Implementar el ordenamiento de la lista en java
```
[54,53,2,1,5,98,73,86,98,94,1,2,3,2]
```


### 5.- Ejercicio
Implementar en Angular 8 , MYSQL , Spring Boot y Docker el siguientes casos de uso:

* Actualmente los clientes cuentan con una tarjeta electrónica, donde realizan recargas y comprar en cualquier punto de venta.

* Se solicita crear los siguientes casos de uso.
* Permitir agregar saldos si el cliente esta activo
* Permitir comprar un producto si este cuenta con saldo.
* Permitir y consultar el histórico de comprar y recargas.
* Permitir la edición del nombre del cliente.

*  Vistas
```

1.- Cliente
Nombre : Eduardo Padilla
Cuenta: 12234444

2.- Compras
Productos: Coca
Precio : 3500

3.- Recarga
Cuenta : 12234444
Cantidad : 5000

4.- Histórico
   Eduardo Padilla  
   Cta :12234444 

```
 Vista Histórico
 
|Descripción| Compras| Recargas|
| --- | --- |--- |
|Recarga	| 		 | 5000    |
|Coca		| 3500	 |         |
|Papas		| 1500	 |         |
|Recarga	|        |  1000   |
|TOTAL	    |   5000 |  6000   |
|Saldo	    |    |  	+1000   |

### 6.- Finalizar ejercicio
* Deben de generar un archivo "Ejercicio.md" , especificando las instrucciones de ejecución de los   proyectos. 

* Deben crear un pull request con su nombre completo.
* Tiene hasta el  día 25/05/2020 a las 9:00 am como máximo para la entrega.


